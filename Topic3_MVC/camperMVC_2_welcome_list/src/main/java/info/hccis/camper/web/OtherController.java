package info.hccis.camper.web;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    @RequestMapping("/")
    public String showHome(Model model) {

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found "+campers.size()+" campers.  Going to welcome page");
        model.addAttribute("campers", campers);
        
        //This will send the user to the welcome.html page.
        return "other/welcome";
    }


}
