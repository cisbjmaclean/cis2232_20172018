package info.hccis.web;

import info.hccis.camper.bo.CamperValidationBO;
import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.entity.Camper;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    @RequestMapping("/camper/add")
    public String add(Model model) {

        Camper camper = new Camper();
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    @RequestMapping("/camper/addSubmit")
    public String addSubmit(Model model, @ModelAttribute("camper") Camper camper) {

        ArrayList<String> errors = CamperValidationBO.validateCamper(camper);
        //If there is an error send them back to add page.
        
        boolean error = false;
        if(!errors.isEmpty()){
            error = true;
        }
        
        if (error) {
            model.addAttribute("messages", errors);
            return "/camper/add";
        }

        System.out.println("BJM - About to add " + camper + " to the database");
        try {
            CamperDAO.update(camper);
            //Get the campers from the database
            ArrayList<Camper> campers = CamperDAO.selectAll();
            model.addAttribute("campers", campers);
        } catch (Exception ex) {
            System.out.println("BJM - There was an error adding camper to the database");
        }
        return "camper/list";
    }

    @RequestMapping("/camper/list")
    public String showHome(Model model) {

        //Get the campers from the database
        ArrayList<Camper> campers = CamperDAO.selectAll();
        System.out.println("BJM-found " + campers.size() + " campers.  Going to welcome page");
        model.addAttribute("campers", campers);

        //This will send the user to the welcome.html page.
        return "camper/list";
    }

}
