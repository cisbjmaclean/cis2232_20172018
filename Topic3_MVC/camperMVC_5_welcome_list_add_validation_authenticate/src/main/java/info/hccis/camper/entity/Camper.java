package info.hccis.camper.entity;

import java.io.Serializable;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper implements Serializable {

    public static final int NUMBER_CAMPERS = 50;
    public static final int RECORD_SIZE = 150;

//
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;

    public Camper() {
        //do nothing.
    }


    /**
     * Custom constructor with all info
     *
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "RegistrationId=" + registrationId + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob;
    }

}
