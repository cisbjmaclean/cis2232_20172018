drop database canes;
create database canes;
use canes;

/*create a user in database*/
grant select, insert, update, delete, alter on canes.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;


CREATE TABLE `Camper` (
  `id` int(4) UNSIGNED AUTO_INCREMENT NOT NULL COMMENT 'Registration id',
  `firstName` varchar(10) NOT NULL,
  `lastName` varchar(10) NOT NULL,
  `dob` varchar(10) DEFAULT NULL,
  campType int(3) DEFAULT 0,
PRIMARY KEY (`id`)
);

--
-- Dumping data for table `camper`
--

INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(1, 'Nick', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(2, 'Audrey', 'Matheson', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(3, 'Patrick', 'Campbell', '12345',2);
INSERT INTO `Camper` (`id`, `firstName`, `lastName`, `dob`,campType) VALUES
(4, 'Nick', 'Smith', '12345',2);

--
-- Table structure for table CodeType
--

CREATE TABLE CodeType (
  codeTypeId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Table structure for table CodeValue
--

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Table structure for table User
--

CREATE TABLE User (
  userId int(3) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  lastName varchar(100) NOT NULL,
  firstName varchar(100) NOT NULL,
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  additional1 varchar(100),
  additional2 varchar(100),
  createdDateTime varchar(20) DEFAULT NULL COMMENT 'When user was created.'
);


INSERT INTO CodeType (codeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'Admin', 'Admin', 'AdminFR', 'AdminFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN'),
(1, 2, 'General', 'General', 'GeneralFR', 'GeneralFR', sysdate(), 'ADMIN', sysdate(), 'ADMIN');

INSERT INTO User (userId, username, password, lastName, firstName, userTypeCode, additional1, additional2, createdDateTime) VALUES
(1, 'bmaclean', '202cb962ac59075b964b07152d234b70', 'MacLean', 'BJ', 1, NULL, NULL, sysdate());
