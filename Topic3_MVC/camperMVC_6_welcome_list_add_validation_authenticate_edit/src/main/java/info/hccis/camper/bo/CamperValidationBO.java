package info.hccis.camper.bo;

import info.hccis.camper.entity.Camper;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @since Oct 18, 2017
 * @author bjmaclean
 */
public class CamperValidationBO {

    public static ArrayList<String> validateCamper(Camper camper) {

        //Do some validation before trying the update.
        ArrayList<String> messages = new ArrayList();
        //First name required
        if (camper.getFirstName().isEmpty()) {
            messages.add("First name is required");
        }
        //Last name required
        if (camper.getLastName().isEmpty()) {
            messages.add("Last name is required");
        }

        //dob is in yyyy/mm/dd
        boolean dobError = false;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            formatter.setLenient(false);
            formatter.parse(camper.getDob());
        } catch (Exception e) {
            System.out.println("BJM - error parsing date");
            dobError = true;
            messages.add("Date must be in yyyy-MM-dd format");
        }

        if (!dobError) {
            //Validate that the camper is born within last 16 years.
            //Get current year
            //From Stack Overflow
            Calendar now = Calendar.getInstance();   // Gets the current date and time
            int year = now.get(Calendar.YEAR);       // The current year

            //Get their birth year
            int birthYear = Integer.parseInt(camper.getDob().substring(0, 4));

            //Subtract
            if (year - birthYear > 16) {
                messages.add("Camper must be born after " + (year - 17));
            }
        }
        return messages;
    }

}
