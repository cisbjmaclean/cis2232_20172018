package info.hccis.web;

import info.hccis.hello.bo.HelloBO;
import info.hccis.hello.entity.Course;
import info.hccis.hello.entity.Person;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session, HttpServletRequest request) {

        //Add a person to the model
        Person person = new Person();
        person.setEmail("admin2@hccis.info");
        model.addAttribute("person", person);
        
        Course course = new Course();
        session.setAttribute("courses", course.getCourses());
        
        
        
        //This will send the user to the welcome.html page.
        return "hello/welcome";
    }

    @RequestMapping("/sayhello" )
    public String sayHello(Model model, @ModelAttribute("person") Person thePerson) {
        
        HelloBO helloBO = new HelloBO();
        helloBO.sayHelloToConsole(thePerson.getName());        
        helloBO.savePerson(thePerson);
        
        //This will send the user to the welcome.html page.
        return "hello/newOne";
    }

    
    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "hello/newOne";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
