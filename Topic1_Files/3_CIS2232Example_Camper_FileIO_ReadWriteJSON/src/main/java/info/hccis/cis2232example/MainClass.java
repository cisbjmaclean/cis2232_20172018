package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {
    
    public static String MENU = "Options:\nA) Add camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/cis2232/campers.txt";
    
    public static void main(String[] args) throws IOException {
        //Create a folder
        Files.createDirectories(Paths.get("/cis2232"));
        
        ArrayList<Camper> theList = new ArrayList();
        loadCampers(theList);
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();
            
            switch (option) {
                case "A":
                    //System.out.println("Picked A");
                    Camper newCamper = new Camper(true);
                    theList.add(newCamper);

                    //Also write to the file when a new camper was added!!
                    BufferedWriter bw = null;
                    FileWriter fw = null;
                    
                    try {
                        fw = new FileWriter(FILE_NAME, true);
                        bw = new BufferedWriter(fw);

                        //bw.write(newCamper.getCSV(true));
                        //JSON instead
                        bw.write(newCamper.getJson()+System.lineSeparator());
                        System.out.println("Done");
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (bw != null) {
                                bw.close();
                            }
                            
                            if (fw != null) {
                                fw.close();
                            }
                            
                        } catch (IOException ex) {
                            
                            ex.printStackTrace();
                            
                        }
                        
                    }
                    
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    for (Camper camper : theList) {
                        System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
                
            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers) {
        System.out.println("Loading campers from file");
        int count = 0;
        
        try {
            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));
            
            for (String current : test) {
                System.out.println("Loading:  " + current);
                //Get a camper from the json
                
                Gson gson = new Gson();
                Camper temp = gson.fromJson(current, Camper.class);
                if (temp.getRegistrationId() > Camper.getMaxRegistrationId()) {
                    Camper.setMaxRegistrationId(temp.getRegistrationId());
                }
                campers.add(temp);
                count++;
            }
            
        } catch (IOException ex) {
            System.out.println("Error loading campers from file.");
            System.out.println(ex.getMessage());
        }
        
        System.out.println("Finished...Loading campers from file (Loaded " + count + " campers)\n\n");
        
    }
}
