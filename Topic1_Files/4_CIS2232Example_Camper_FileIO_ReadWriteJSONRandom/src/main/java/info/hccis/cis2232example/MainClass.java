package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nA) Add camper\nS) Show campers\nX) Exit";
    public static String FILE_NAME = "/cis2232/campers.txt";
    private static BufferedWriter bw = null;
    private static FileWriter fw = null;
    private static String emptySpaces = "          ";
    private static String emptySpaces100 = "";

    public static void main(String[] args) throws IOException {
        //Create a folder
        Files.createDirectories(Paths.get("/cis2232"));
        File file = new File(FILE_NAME);

        //Build the empty spaces variable which will be used to pad the 
        //json string when updating.
        for (int i = 0; i < 10; i++) {
            emptySpaces100 += emptySpaces;
        }

        //If the file doesn't exist create it and fill it with spaces.
        if (!file.exists()) {
            fw = new FileWriter(FILE_NAME, true);

            for (int i = 0; i < 1000; i++) {
                fw.write(emptySpaces);
            }
            fw.close();
        }

        ArrayList<Camper> theList = new ArrayList();
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {
                case "A":
                    addCamper(theList);
                    break;
                case "S":

                    //Load the campers from the file again to show the
                    //moost recent list.
                    loadCampers(theList);
                    int total = 0;
                    for (Camper camper : theList) {
                        System.out.println(camper);
                    }

                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * Add a camper
     *
     * @since 20170927
     * @author 2232a modified by 2232b
     * @param theList
     */
    public static void addCamper(ArrayList<Camper> theList) {
        //System.out.println("Picked A");
        Camper newCamper = new Camper(true);
        theList.add(newCamper);

        //Also write to the file when a new camper was added!!
        try {
            File file = new File(FILE_NAME);
            RandomAccessFile raf = new RandomAccessFile(file, "rw");

            //Example: Camper registration #3 would start at location 200
            int location = newCamper.getRegistrationId();
            location--;
            location *= 100;
            raf.seek(location);

            /*
                          Have to pad with empty spaces up to 100 bytes to overwrite 
                          the part of the file associated with this camper.
             */
            String temp = (newCamper.getJson() + emptySpaces100.substring(newCamper.getJson().length()));
            System.out.println(temp);
            raf.writeBytes(temp);
            System.out.println(temp.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * This method will load the campers from the file at the program startup.
     *
     * @param campers
     * @since 20150917
     * @author BJ MacLean
     */
    public static void loadCampers(ArrayList campers) {
        //System.out.println("Loading campers from file");
        int count = 0;
        campers.clear();
        try {
            ArrayList<String> test = (ArrayList<String>) Files.readAllLines(Paths.get(FILE_NAME));
            String allCampers = test.get(0);
            Gson gson = new Gson();
            for (int i = 0; i < 100; i++) {
                String current = allCampers.substring(i * 100, i * 100 + 100);

                if (!current.trim().isEmpty()) {
                    //System.out.println("Loading:  " + current);
                    Camper temp = gson.fromJson(current, Camper.class);
                    if (temp.getRegistrationId() > Camper.getMaxRegistrationId()) {
                        Camper.setMaxRegistrationId(temp.getRegistrationId());
                    }
                    campers.add(temp);
                    count++;
                }

//Get a camper from the json
            }

        } catch (IOException ex) {
            System.out.println("Error loading campers from file.");
            System.out.println(ex.getMessage());
        }

        //System.out.println("Finished...Loading campers from file (Loaded " + count + " campers)\n\n");

    }
}
