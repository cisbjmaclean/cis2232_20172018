package info.hccis.cis2232example;

import com.google.gson.Gson;
import java.util.Scanner;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper {

    private static int maxRegistrationId;
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;

    public Camper() {
        //do nothing.
    }

    /**
     * Default constructor which will get info from user
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(boolean getFromUser) {
        System.out.println("Enter first name");
        this.firstName = FileUtility.getInput().nextLine();

        System.out.println("Enter last name");
        this.lastName = FileUtility.getInput().nextLine();

        System.out.println("Enter dob");
        this.dob = FileUtility.getInput().nextLine();

        System.out.println("Please enter your registration id or 0 if don't have one");
        this.registrationId = FileUtility.getInput().nextInt();
        FileUtility.getInput().nextLine(); //burn the line
        if (registrationId == 0) {
            this.registrationId = ++maxRegistrationId;
        }
    }

    /**
     * Custom constructor with all info
     *
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    /**
     * constructor which will create from String array
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String[] parts) {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
        /*
         This makes sure that we capture/set the maximum registration id as we load
         all of the entries from the file.  Then when we add a new camper it will
         use this to set the next registration id.
         */
        if (Integer.parseInt(parts[0]) > maxRegistrationId) {
            maxRegistrationId = Integer.parseInt(parts[0]);
        }
    }

    /**
     * constructor which will create from String array
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String csvValues) {
        this(csvValues.split(","));
    }

    public String getCSV() {
        return registrationId + "," + firstName + "," + lastName + "," + dob;
    }

    /**
     * This will give back the Camper details
     *
     * @param withLineFeed Return with a line feed character
     * @return camper details
     * @since 20170915
     * @author cis2232
     */
    public String getCSV(boolean withLineFeed) {
        if (withLineFeed) {
            return getCSV() + System.lineSeparator();
        } else {
            return getCSV();
        }
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public static int getMaxRegistrationId() {
        return maxRegistrationId;
    }

    public static void setMaxRegistrationId(int maxRegistrationId) {
        Camper.maxRegistrationId = maxRegistrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "RegistrationId=" + registrationId + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob;
    }

}
