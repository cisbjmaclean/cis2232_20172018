package info.hccis.bo;

import com.google.gson.Gson;
import info.hccis.util.FileUtility;
import info.hccis.dao.CamperDAO;
import java.io.Serializable;

/**
 * This class will represent a camper.
 *
 * @author bjmaclean
 * @since 20150915
 */
public class Camper implements Serializable {

    public static final int NUMBER_CAMPERS = 50;
    public static final int RECORD_SIZE = 150;

//
    private int registrationId;
    private String firstName;
    private String lastName;
    private String dob;

    public Camper() {
        //do nothing.
    }

    public void getDetails() {
        System.out.println("Enter first name");
        this.firstName = FileUtility.getInput().nextLine();

        System.out.println("Enter last name");
        this.lastName = FileUtility.getInput().nextLine();

        System.out.println("Enter dob");
        this.dob = FileUtility.getInput().nextLine();
    }

    /**
     * Default constructor which will get info from user
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(boolean getFromUser) {
        getDetails();
    }

    /**
     * Custom constructor with all info
     *
     * @param registrationId
     * @param firstName
     * @param lastName
     * @param dob
     *
     * @author BJ MacLean
     * @since 20150917
     */
    public Camper(int registrationId, String firstName, String lastName, String dob) {
        this.registrationId = registrationId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    /**
     * constructor which will create from csvString
     *
     * @since 20150917
     * @author BJ MacLean
     */
    public Camper(String[] parts) {
        this(Integer.parseInt(parts[0]), parts[1], parts[2], parts[3]);
        /*
         This makes sure that we capture/set the maximum registration id as we load
         all of the entries from the file.  Then when we add a new camper it will
         use this to set the next registration id.
         */

    }

    public void edit() {

        System.out.println("**************************************");
        System.out.println("Here's the existing info for this guy:");
        Camper reloadedCamper = CamperDAO.select(this.registrationId);
        this.dob = reloadedCamper.getDob();
        this.firstName = reloadedCamper.getFirstName();
        this.lastName = reloadedCamper.getLastName();

        System.out.println(this.toString());
        System.out.println("**************************************");

        getDetails();
    }

    public String getCSV() {
        return registrationId + "," + firstName + "," + lastName + "," + dob;
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "RegistrationId=" + registrationId + ", firstName=" + firstName + ", lastName=" + lastName + ", dob=" + dob;
    }

}
