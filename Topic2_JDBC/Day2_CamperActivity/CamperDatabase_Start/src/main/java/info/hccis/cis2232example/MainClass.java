package info.hccis.cis2232example;

import info.hccis.util.FileUtility;
import info.hccis.bo.Camper;
import info.hccis.dao.CamperDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bjmaclean
 */
public class MainClass {

    public static String MENU = "Options:\nE) Edit (or Add) camper\nS) Show campers\nY) Show campers by DOB\nX) Exit";
    //public static String FILE_NAME = "/test/campersRandom.txt";

    public static void main(String[] args) throws IOException {

        String option = "";

        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();

            switch (option) {

                case "E":
                    System.out.println("Enter id (0 to add)");
                    int id = Integer.parseInt(FileUtility.getInput().nextLine());

                    Camper theCamper = null;
                    if (id == 0) {
                        theCamper = new Camper(true);
                    } else {
                        theCamper = CamperDAO.select(id);
                        theCamper.edit();
                    }

                    try {
                        CamperDAO.update(theCamper);
                    } catch (Exception ex) {
                        Logger.getLogger(MainClass.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //FileUtility.writeCamperRandom(theCamper, FILE_NAME);
//                    FileUtility.writeLine(FILE_NAME, newCamper.getJson());
                    break;
                case "S":
                    System.out.println("Here are the campers");
                    ArrayList<Camper> theList = loadCampersFromDatabase();
                    for (Camper camper : loadCampersFromDatabase()) {
                        System.out.println(camper);
                    }
                    break;
                case "Y":
                    System.out.println("Enter year of birth:");
                    String year = FileUtility.getInput().nextLine();

                    System.out.println("Here are the campers");
                    //theList = loadCampersFromDatabase(theList);
                    for (Camper camper : CamperDAO.selectAllByDOB(year)) {
                        System.out.println(camper);
                    }
                    break;
                case "X":
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid option");
                    break;

            }
        } while (!option.equalsIgnoreCase("x"));
    }

    /**
     * Load all the campers from the database and set the campers into the
     * arraylist.
     *
     * @since 20160930
     * @author BJM
     * @param campers
     */
    public static ArrayList<Camper> loadCampersFromDatabase() {

        ArrayList<Camper> theList = new ArrayList();
        //Functionality to be completed.
        
        //Create the method in the CamperDAO which will return an ArrayList to this
        //method which will in turn return it to the calling method.

        return theList;
    }

}
