package info.hccis.admin.web.services;

import info.hccis.admin.dao.CodeValueDAO;
import info.hccis.admin.dao.UserDAO;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.User;
import info.hccis.admin.service.CodeService;
import info.hccis.admin.util.Utility;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Component
@Path("/code")
@Scope("request")
public class CodesRest {
    @Resource
    private final CodeService cs;

    public CodesRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.cs = applicationContext.getBean(CodeService .class);
    }
    
    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String theId) {

        String output = "Jersey say : " + theId;
        System.out.println("in Jersey hello web service");

        output = cs.getCodeTypes().toString();// ctr.findOne(Integer.parseInt(theId)).getEnglishDescription();

        return Response.status(200).entity(output).build();

    }
    
    @GET
    @Path("values/{db}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("db") String dbName, @PathParam("id") int id) {

         DatabaseConnection databaseConnection = new DatabaseConnection();
         databaseConnection.setDatabaseName(dbName);
        ArrayList<info.hccis.admin.model.jpa.CodeValue> cvs= CodeValueDAO.getCodeValues(databaseConnection, String.valueOf(id));
        
        if(cvs == null || cvs.size()==0){
            return Response.status(204).entity("{}").header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(cvs);
        } catch (IOException ex) {
            Logger.getLogger(CodesRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).header("Access-Control-Allow-Origin", "*")
      .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }

}
